<?php

locate_template('includes/wp_booster/td_single_template_vars.php', true);

get_header();

global $loop_module_id, $loop_sidebar_position, $post, $td_sidebar_position;

$td_mod_single = new td_module_single($post);


?>
<div class="td-main-content-wrap td-container-wrap">

    <div class="td-container td-post-template-default <?php echo $td_sidebar_position; ?>">
        <div class="td-crumb-container"><?php echo td_page_generator::get_single_breadcrumbs($td_mod_single->title); ?></div>

        <div class="td-pb-row">
            <?php

            //the default template
            switch ($loop_sidebar_position) {
                default: //sidebar right
					?>
                        <div class="td-pb-span8 td-main-content" role="main">
                            <div class="td-ss-main-content">

                            <?php
                                $current_user = wp_get_current_user();
                                if ( 0 == $current_user->ID ) { ?>

                                <div class="td-post-content">
                                  <?php
                                  // override the default featured image by the templates (single.php and home.php/index.php - blog loop)
                                  if (!empty(td_global::$load_featured_img_from_template)) {
                                      echo $td_mod_single->get_image(td_global::$load_featured_img_from_template);
                                  } else {
                                      echo $td_mod_single->get_image('td_696x0');
                                  }
                                  ?>

                                  <div class="td_module_1 logged-msg">
                                    <p class="h3">
                                      Por favor <a class="td-login-modal-js menu-item" href="#login-form" data-effect="mpf-td-login-effect"> regístrate o inicia sesión</a>
                                      para poder ver esta publicación.
                                    </p>
                                  </div>
                              </div>
                                   

                                <?php } else { ?>
                                    
                                  <?php
                                    locate_template('loop-single.php', true);
                                    comments_template('', true);
                                  ?>

                                <?php }
                                ?>

                                <?php
                                // locate_template('loop-single.php', true);
                                comments_template('', true);
                                ?>
                            </div>
                        </div>
                        <div class="td-pb-span4 td-main-sidebar" role="complementary">
                            <div class="td-ss-main-sidebar">
                                <?php get_sidebar(); ?>
                            </div>
                        </div>
                    <?php
                    break;

                case 'sidebar_left':
                    ?>
                        <div class="td-pb-span8 td-main-content <?php echo $td_sidebar_position; ?>-content" role="main">
                            <div class="td-ss-main-content">

                                <div class="td-post-content">
                                  <?php
                                  // override the default featured image by the templates (single.php and home.php/index.php - blog loop)
                                  if (!empty(td_global::$load_featured_img_from_template)) {
                                      echo $td_mod_single->get_image(td_global::$load_featured_img_from_template);
                                  } else {
                                      echo $td_mod_single->get_image('td_696x0');
                                  }
                                  ?>
                              </div>
                                   

                                <?php
                                  $category = get_the_category(); 
                                  $category_parent_id = $category[0]->category_parent;
                                  if( $category_parent_id == 64 || $category_parent_id == 70 ) {  ?>
                                    <?php
                                  $current_user = wp_get_current_user();
                                  if ( 0 == $current_user->ID ) { ?>
                                    <div class="td_module_1 logged-msg">
                                      <p class="h3">
                                        Por favor <a class="td-login-modal-js menu-item" href="#login-form" data-effect="mpf-td-login-effect"> regístrate o inicia sesión</a>
                                        para poder ver esta publicación.
                                      </p>
                                    </div>
                                  <?php } else { ?>
                                    <?php
                                      locate_template('loop-single-13.php', true);
                                    ?>
                                  <?php } ?>
                                <?php } else {  ?> 
                                  <?php
                                    locate_template('loop-single-13.php', true);
                                    comments_template('', true);
                                  ?>
                                <?php }  ?>

                            </div>
                        </div>
		                <div class="td-pb-span4 td-main-sidebar" role="complementary">
			                <div class="td-ss-main-sidebar">
				                <?php get_sidebar(); ?>
			                </div>
		                </div>
                    <?php
                    break;

                case 'no_sidebar':
                    td_global::$load_featured_img_from_template = 'td_1068x0';
                    ?>
                        <div class="td-pb-span12 td-main-content" role="main">
                            <div class="td-ss-main-content">

                            <?php
                                $current_user = wp_get_current_user();
                                if ( 0 == $current_user->ID ) { ?>

                                <div class="td-post-content">
                                  <?php
                                  // override the default featured image by the templates (single.php and home.php/index.php - blog loop)
                                  if (!empty(td_global::$load_featured_img_from_template)) {
                                      echo $td_mod_single->get_image(td_global::$load_featured_img_from_template);
                                  } else {
                                      echo $td_mod_single->get_image('td_696x0');
                                  }
                                  ?>

                                  <div class="td_module_1 logged-msg">
                                    <p class="h3">
                                      Por favor <a class="td-login-modal-js menu-item" href="#login-form" data-effect="mpf-td-login-effect"> regístrate o inicia sesión</a>
                                      para poder ver esta publicación.
                                    </p>
                                  </div>
                              </div>
                                   

                                <?php } else { ?>
                                    
                                  <?php
                                    locate_template('loop-single.php', true);
                                    comments_template('', true);
                                  ?>

                                <?php }
                                ?>
                                
                                <?php
                                // locate_template('loop-single.php', true);
                                comments_template('', true);
                                ?>
                            </div>
                        </div>
                    <?php
                    break;

            }
            ?>
        </div> <!-- /.td-pb-row -->
    </div> <!-- /.td-container -->
</div> <!-- /.td-main-content-wrap -->

<?php

get_footer();