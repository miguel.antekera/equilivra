<?php

//
if ( ! is_super_admin() ) {
    add_filter( 'show_admin_bar', '__return_false' );
}

//
function buddydev_author_redirect_to_profile() {
 
    if ( is_author() && function_exists( 'bp_core_redirect' ) ) {
 
        $author_id = get_queried_object_id();
        bp_core_redirect( bp_core_get_user_domain( $author_id ) );
    }
}
add_action( 'template_redirect', 'buddydev_author_redirect_to_profile' );

//
add_action('wp_logout','auto_redirect_after_logout');
  function auto_redirect_after_logout(){
    wp_redirect( home_url() );
  exit();
}

//
$required_capability = 'edit_others_posts';
$redirect_to = '';
function no_admin_init() {      
    // We need the config vars inside the function
    global $required_capability, $redirect_to;      
    // Is this the admin interface?
    if (
        // Look for the presence of /wp-admin/ in the url
        stripos($_SERVER['REQUEST_URI'],'/wp-admin/') !== false
        &&
        // Allow calls to async-upload.php
        stripos($_SERVER['REQUEST_URI'],'async-upload.php') == false
        &&
        // Allow calls to admin-ajax.php
        stripos($_SERVER['REQUEST_URI'],'admin-ajax.php') == false
    ) {         
        // Does the current user fail the required capability level?
        if (!current_user_can($required_capability)) {              
            if ($redirect_to == '') { $redirect_to = get_option('home'); }              
            // Send a temporary redirect
            wp_redirect($redirect_to,302);              
        }           
    }       
}
// Add the action with maximum priority
add_action('init','no_admin_init',0);


 
?>
