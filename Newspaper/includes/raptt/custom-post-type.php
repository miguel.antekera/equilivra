<?php

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}

  


function custom_post_vitrina() {
	register_post_type( 'vitrina', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		array( 'labels' => array(
			'name' => __( 'Vitrina', 'sage' ),
			'singular_name' => __( 'vitrina', 'sage' ),
			'all_items' => __( 'Todos', 'sage' ),
			'add_new' => __( 'Agregar nueva', 'sage' ),
			'add_new_item' => __( 'Agregar nueva vitrina', 'sage' ),
			'edit' => __( 'Editar', 'sage' ),
			'edit_item' => __( 'Editar vitrina', 'sage' ),
			'new_item' => __( 'Nueva vitrina', 'sage' ),
			'view_item' => __( 'Ver vitrina', 'sage' ),
			'search_items' => __( 'Buscar vitrina', 'sage' ),
			'not_found' =>  __( 'No hay vitrina agregados.', 'sage' ),
			'not_found_in_trash' => __( 'Vacío', 'sage' ),
			'parent_item_colon' => ''
			),
			'description' => __( 'vitrina', 'sage' ),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'show_in_rest' => true,
      'menu_position' => 8,
			'menu_icon' => 'dashicons-admin-generic',
			'rewrite'	=> array( 'slug' => 'vitrina', 'with_front' => true ),
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'author', 'revisions', 'thumbnail', 'excerpt')
		) /* end of options */
	); /* end of register post type */

	/* this adds your post categories to your custom post type */
  // register_taxonomy_for_object_type( 'category', 'vitrina' );
	/* this adds your post tags to your custom post type */
	// register_taxonomy_for_object_type( 'post_tag', 'vitrina' );

}

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_vitrina');
 




  // now let's add custom categories (these act like categories)
  register_taxonomy( 'custom_cat_vitrina',
    array('vitrina'), /* if you change the name of register_post_type( 'vitrina', then you have to change this */
    array('hierarchical' => true,     /* if this is true, it acts like categories */
      'labels' => array(
        'name' => __( 'Categorías', 'sage' ),
        'description' => __( 'Categorías', 'sage' ),
        'singular_name' => __( 'Categoría', 'sage' ),
        'search_items' =>  __( 'Buscar Categoría', 'sage' ),
        'all_items' => __( 'Todas', 'sage' ),
        'parent_item' => __( 'Parent Custom Category', 'sage' ),
        'parent_item_colon' => __( 'Parent Custom Category:', 'sage' ),
        'edit_item' => __( 'Editar', 'sage' ),
        'update_item' => __( 'Update', 'sage' ),
        'add_new_item' => __( 'Add New', 'sage' ),
        'new_item_name' => __( 'Nueva categoría', 'sage' )
      ),
      'show_admin_column' => true,
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => 'categoria-vitrina' ),
      'public' => false,
    )
  );

  

?>
